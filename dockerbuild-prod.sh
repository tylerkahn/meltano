#!/usr/bin/env bash
set -e

BASE_IMAGE=$1
AWS_PROFILE=bandalier-data
ECR_HOST=597337394473.dkr.ecr.us-east-2.amazonaws.com
ECR_URI=$ECR_HOST/bandalier/meltano-fork
IMAGE_NAME=bandalier/meltano-fork

echo "You must have a ~/.aws/credentials profile called bandalier-data set up for the bandalier-data user."

aws ecr get-login-password --region us-east-2 --profile $AWS_PROFILE | docker login --username AWS --password-stdin $ECR_HOST
docker build \
  --file docker/prod/Dockerfile \
  -t $IMAGE_NAME \
  --build-arg BASE_IMAGE=$BASE_IMAGE \
  .

GIT_HASH=`git rev-parse --short=16 HEAD`
GIT_DIRTY_SUFFIX=`test -z "$(git status --porcelain)" || (echo -n "-dirty-" && date +"%Y-%m-%dT%H-%M-%S%Z")`
GIT_SUFFIX=$GIT_HASH$GIT_DIRTY_SUFFIX

echo "Tagging with $GIT_SUFFIX"
docker tag $IMAGE_NAME:latest $ECR_URI:latest

docker tag $IMAGE_NAME:latest $IMAGE_NAME:$GIT_SUFFIX
docker tag $IMAGE_NAME:latest $ECR_URI:$GIT_SUFFIX

echo "Pushing to ECR"
docker push $ECR_URI:latest
docker push $ECR_URI:$GIT_SUFFIX
